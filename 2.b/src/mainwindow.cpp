#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLineEdit.h>
#include <qslider.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::changeColorRed()
{
	int slider1Value = ui->horizontalSlider->value();
	int slider2Value = ui->horizontalSlider_2->value();
	int slider3Value = ui->horizontalSlider_3->value();
	QPalette pal = palette();
	pal.setColor(QPalette::Background, QColor(slider1Value, slider2Value, slider3Value));
	ui->centralWidget->setAutoFillBackground(true);
	ui->centralWidget->setPalette(pal);
}
void MainWindow::changeColorGreen()
{
	int slider1Value = ui->horizontalSlider->value();
	int slider2Value = ui->horizontalSlider_2->value();
	int slider3Value = ui->horizontalSlider_3->value();
	QPalette pal = palette();
	pal.setColor(QPalette::Background, QColor(slider1Value, slider2Value, slider3Value));
	ui->centralWidget->setAutoFillBackground(true);
	ui->centralWidget->setPalette(pal);
}
void MainWindow::changeColorBlue()
{
	int slider1Value = ui->horizontalSlider->value();
	int slider2Value = ui->horizontalSlider_2->value();
	int slider3Value = ui->horizontalSlider_3->value();
	QPalette pal = palette();
	pal.setColor(QPalette::Background, QColor(slider1Value, slider2Value, slider3Value));
	ui->centralWidget->setAutoFillBackground(true);
	ui->centralWidget->setPalette(pal);
}