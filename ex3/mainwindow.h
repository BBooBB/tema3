#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPlainTextEdit>
#include <qfileinfo.h>
#include <qstring.h>
#include <qvector.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

	public slots:

	//eveniment pt tab nou 
	void newFile();

	//eveniment pt deschidere fisier intr-un tab nou
	void openFile();

	//eveniment pt save as
	void saveAsFile(bool);

	//eveniment pt save
	void saveFile(bool);

private:
    Ui::MainWindow *ui;

	//nr total de taburi
	int contorTabs;

	//lista cu numele fisierelor salvate
	QVector<QString> *filesSaved;

	//lista cu caile acestor fisiere
	QVector<QFileInfo> *pathsSaved;
};

#endif // MAINWINDOW_H
