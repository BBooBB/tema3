#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>
#include <QVBoxLayout> 
#include <QFileDialog>
#include <fstream>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qplaintextedit.h>
#include <string.h>
#include <qstring.h>
#include <qfile.h>
#include <qtextstream.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	
	contorTabs = 1;

	//legatura intre new,open,save,save as si evenimentele asociate fiecaruia

	connect(ui->action_New, SIGNAL(triggered(bool)), this, SLOT(newFile()));

	connect(ui->action_Open, SIGNAL(triggered(bool)), this, SLOT(openFile()));

	connect(ui->action_Save, SIGNAL(triggered(bool)),
		this, SLOT(saveFile(bool)));

	connect(ui->action_Save_As, SIGNAL(triggered(bool)),
		this, SLOT(saveAsFile(bool)));


	filesSaved = new QVector<QString>();
	pathsSaved = new QVector<QFileInfo>();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newFile()
{
	contorTabs++;
	QPlainTextEdit *editor = new QPlainTextEdit();
	ui->tabWidget->addTab(editor,"New"+QString::number(contorTabs));
	ui->tabWidget->setCurrentWidget(editor);
}

void MainWindow::openFile()
{
	QFileInfo filePath = QFileDialog::getOpenFileName(this, tr("Open File"), "", tr("Text files (*.txt)"));
	QString filename = filePath.fileName();
	if (!filename.isEmpty())
	{
		contorTabs++;

		QFile file(filePath.absoluteFilePath());
		file.open(QIODevice::ReadOnly);
		QTextStream data(&file);

		QPlainTextEdit *edit = new QPlainTextEdit(ui->centralWidget);
		edit->setPlainText(data.readAll());
		ui->tabWidget->addTab(edit, filename);
		ui->tabWidget->setCurrentWidget(edit);

		pathsSaved->append(filePath);
		filesSaved->append(filename);
	}
}

void MainWindow::saveAsFile(bool)
{
	QFileInfo filePath = QFileDialog::getSaveFileName(this, tr("Save File"), "", tr("Text files (*.txt)"));
	QString filename = filePath.fileName();
	int index = ui->tabWidget->currentIndex();
	ui->tabWidget->setTabText(index, filename);

	pathsSaved->append(filePath);
	filesSaved->append(filename);

	std::ofstream out(filePath.absoluteFilePath().toStdString());
	QPlainTextEdit *text = new QPlainTextEdit(ui->tabWidget->currentWidget());
	out << text->toPlainText().toStdString();
}

void MainWindow::saveFile(bool)
{
	int index = ui->tabWidget->currentIndex();
	QString header = ui->tabWidget->tabText(index);
	if (filesSaved->contains(header))
	{
		int position = filesSaved->indexOf(header);
		QFileInfo path = pathsSaved->at(position);

		std::ofstream out(path.absoluteFilePath().toStdString());
		QPlainTextEdit *text = new QPlainTextEdit(ui->tabWidget->currentWidget());
		out << text->toPlainText().toStdString();
	}
	else
		MainWindow::saveAsFile(true);
}
